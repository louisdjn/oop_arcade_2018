/*
** EPITECH PROJECT, 2018
** OOP_arcade_2018
** File description:
** Color.hpp
*/

#ifndef OOP_ARCADE_2018_COLOR_HPP
# define OOP_ARCADE_2018_COLOR_HPP

namespace arcade {
    typedef enum {
        ARCADE_TRANSPARENT, // 0
        ARCADE_WHITE, // 1
        ARCADE_BLACK, // 2
        ARCADE_RED, // 3
        ARCADE_BLUE, // 4
        ARCADE_GREEN, // 5
        ARCADE_YELLOW, // 6
        ARCADE_MAGENTA, // 7
        ARCADE_CYAN // 8
    } Color;
}

#endif /* OOP_ARCADE_2018_COLOR_HPP */
