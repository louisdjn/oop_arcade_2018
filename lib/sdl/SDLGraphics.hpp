/*
** EPITECH PROJECT, 2022
** OOP_arcade_2018
** File description:
** Created by qjean-christophe,
*/

#ifndef GRAPHIC_HPP
#define GRAPHIC_HPP

#include "SDLUtils.hpp"

#define PIXEL_SIZE  20

namespace arcade {
    class SDLGraphic : public IGraphic {
    public:
        SDLGraphic();
        ~SDLGraphic();

        Event getEvent() final;

        void clearWindow(Color color = ARCADE_BLACK) final;

        void displayWindow() final;

        void printPixel(Vector pos, Color color = ARCADE_WHITE) final;

        void printRectangle(Vector pos, Vector size,
            Color color = ARCADE_WHITE) final;

        void printText(Vector v, const std::string &text,
            Color color = ARCADE_WHITE,
                       Color background = ARCADE_TRANSPARENT) final;
    private:
        SDL_Window *_window;
        SDL_Surface *_screenSurface;
        SDL_Renderer *_renderer;
        TTF_Font *_font;
        static std::map<int, arcade::EventType> _keyEquivalences;
        int mousePress(SDL_MouseButtonEvent &b);
    };
}

#endif