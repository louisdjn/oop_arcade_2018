/*
** EPITECH PROJECT, 2022
** OOP_arcade_2018
** File description:
** Created by qjean-christophe,
*/

#include "SDLGraphics.hpp"

const int SCREEN_WIDTH = 1600;
const int SCREEN_HEIGHT = 900;

void __attribute__((constructor)) ctor()
{
    std::cout << "[Arcade] SDL graphic library loaded!" << std::endl;
}

void __attribute__((destructor)) dtor()
{
    std::cout << "[Arcade] SDL graphic library closed!" << std::endl;
}

extern "C" arcade::IGraphic *init()
{
    return new arcade::SDLGraphic();
}

arcade::SDLGraphic::SDLGraphic()
{
    TTF_Init();
    SDL_Init(SDL_INIT_VIDEO);
    _window = SDL_CreateWindow( "Arcade - SDL Edition", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,SDL_WINDOW_SHOWN );
    _screenSurface = SDL_GetWindowSurface( _window );
    _renderer = SDL_CreateRenderer(_window, -1, 0);
    _font = TTF_OpenFont("./assets/font.ttf", 20);
}

arcade::SDLGraphic::~SDLGraphic()
{
    SDL_DestroyWindow( _window );
    _window = NULL;
    SDL_Quit();
}

std::map<int, arcade::EventType> arcade::SDLGraphic::_keyEquivalences = {
    { SDLK_a, arcade::ARCADE_KEY_A},
    { SDLK_b, arcade::ARCADE_KEY_B},
    { SDLK_c, arcade::ARCADE_KEY_C},
    { SDLK_d, arcade::ARCADE_KEY_D},
    { SDLK_e, arcade::ARCADE_KEY_E},
    { SDLK_f, arcade::ARCADE_KEY_F},
    { SDLK_g, arcade::ARCADE_KEY_G},
    { SDLK_h, arcade::ARCADE_KEY_H},
    { SDLK_i, arcade::ARCADE_KEY_I},
    { SDLK_j, arcade::ARCADE_KEY_J},
    { SDLK_k, arcade::ARCADE_KEY_K},
    { SDLK_l, arcade::ARCADE_KEY_L},
    { SDLK_m, arcade::ARCADE_KEY_M},
    { SDLK_n, arcade::ARCADE_KEY_N},
    { SDLK_o, arcade::ARCADE_KEY_O},
    { SDLK_p, arcade::ARCADE_KEY_P},
    { SDLK_q, arcade::ARCADE_KEY_Q},
    { SDLK_r, arcade::ARCADE_KEY_R},
    { SDLK_s, arcade::ARCADE_KEY_S},
    { SDLK_t, arcade::ARCADE_KEY_T},
    { SDLK_u, arcade::ARCADE_KEY_U},
    { SDLK_v, arcade::ARCADE_KEY_V},
    { SDLK_w, arcade::ARCADE_KEY_W},
    { SDLK_x, arcade::ARCADE_KEY_X},
    { SDLK_y, arcade::ARCADE_KEY_Y},
    { SDLK_z, arcade::ARCADE_KEY_Z},
    { SDLK_0, arcade::ARCADE_KEY_0},
    { SDLK_1, arcade::ARCADE_KEY_1},
    { SDLK_2, arcade::ARCADE_KEY_2},
    { SDLK_3, arcade::ARCADE_KEY_3},
    { SDLK_4, arcade::ARCADE_KEY_4},
    { SDLK_5, arcade::ARCADE_KEY_5},
    { SDLK_6, arcade::ARCADE_KEY_6},
    { SDLK_7, arcade::ARCADE_KEY_7},
    { SDLK_8, arcade::ARCADE_KEY_8},
    { SDLK_9, arcade::ARCADE_KEY_9},
    { SDLK_F1, arcade::ARCADE_KEY_F1},
    { SDLK_F2, arcade::ARCADE_KEY_F2},
    { SDLK_F3, arcade::ARCADE_KEY_F3},
    { SDLK_F4, arcade::ARCADE_KEY_F4},
    { SDLK_F5, arcade::ARCADE_KEY_F5},
    { SDLK_F6, arcade::ARCADE_KEY_F6},
    { SDLK_F7, arcade::ARCADE_KEY_F7},
    { SDLK_F8, arcade::ARCADE_KEY_F8},
    { SDLK_F9, arcade::ARCADE_KEY_F9},
    { SDLK_F10, arcade::ARCADE_KEY_F10},
    { SDLK_F11, arcade::ARCADE_KEY_F11},
    { SDLK_F12, arcade::ARCADE_KEY_F12},
    { SDLK_TAB, arcade::ARCADE_KEY_TAB},
    { SDLK_SPACE, arcade::ARCADE_KEY_SPACE},
    { SDLK_ESCAPE, arcade::ARCADE_KEY_ESCAPE},
    { SDLK_RETURN, arcade::ARCADE_KEY_ENTER},
    { SDLK_BACKSPACE, arcade::ARCADE_KEY_DELETE},
    { SDLK_RIGHT, arcade::ARCADE_KEY_RIGHT},
    { SDLK_LEFT, arcade::ARCADE_KEY_LEFT},
    { SDLK_UP, arcade::ARCADE_KEY_UP},
    { SDLK_DOWN, arcade::ARCADE_KEY_DOWN},
};

int arcade::SDLGraphic::mousePress(SDL_MouseButtonEvent& b) {
    if(b.button == SDL_BUTTON_LEFT)
        return 0;
    if(b.button == SDL_BUTTON_RIGHT)
        return 1;
    return -1;
}

arcade::Event arcade::SDLGraphic::getEvent()
{
    SDL_Event event;

    if (!SDL_PollEvent(&event))
        return ARCADE_NO_EVENT;

    switch (event.type) {

        case SDL_WINDOWEVENT :
            if (event.window.event == SDL_WINDOWEVENT_CLOSE)
                return ARCADE_CLOSE_WINDOW;
            break;

        case SDL_KEYDOWN :
            if (_keyEquivalences.find(event.key.keysym.sym) !=
            _keyEquivalences.end())
                return _keyEquivalences[event.key.keysym.sym];
            break;

        case SDL_MOUSEBUTTONDOWN :
            if (mousePress(event.button) == 0)
                return ARCADE_CLICK_LEFT;
            if (mousePress(event.button) == 1)
                return ARCADE_CLICK_RIGHT;
            break;
    }
    return ARCADE_NO_EVENT;
}

void arcade::SDLGraphic::clearWindow(arcade::Color color)
{
    if (_renderer != nullptr)
        SDL_RenderClear(_renderer);
    for (int y = 0; y < SCREEN_HEIGHT / PIXEL_SIZE + 1; y++)
        for (int x = 0; x < SCREEN_WIDTH / PIXEL_SIZE + 1; x++)
            printPixel({ x, y }, color);
}

void arcade::SDLGraphic::displayWindow()
{
    if (_renderer != nullptr)
        SDL_RenderPresent(_renderer);
}

void arcade::SDLGraphic::printPixel(Vector pos, Color color)
{
    printRectangle(pos, {1, 1}, color);
}

void arcade::SDLGraphic::printRectangle(arcade::Vector pos, arcade::Vector size,
    arcade::Color color)
{
    SDL_Rect rect;

    if (!_renderer || color == ARCADE_TRANSPARENT)
        return;
    SDL_Color sdlColor = SDLUtils::toSDLColor(color);
    SDL_SetRenderDrawColor(_renderer, sdlColor.r, sdlColor.g, sdlColor.b, sdlColor.a);

    rect.x = pos.x * PIXEL_SIZE;
    rect.y = pos.y * PIXEL_SIZE;
    rect.w = size.x * PIXEL_SIZE;
    rect.h = size.y * PIXEL_SIZE;

    SDL_RenderFillRect(_renderer, &rect);
    SDL_RenderDrawRect(_renderer, &rect);
}

void arcade::SDLGraphic::printText(arcade::Vector v, const std::string &text,
    arcade::Color color, arcade::Color background)
{
    if (!_renderer)
        return;

    printRectangle(v, {(int) ceil(text.size() / 2.0), 1}, background);
    for (unsigned int i = 0; i < text.size(); i++) {
        int width;
        int height;

        std::stringstream ss;
        ss << text[i] << '\0';

        SDL_Surface *surfaceMessage = TTF_RenderText_Blended(_font, ss.str().c_str(),
                                                             SDLUtils::toSDLColor(color));
        SDL_Texture* Message = SDL_CreateTextureFromSurface(_renderer,
                                                            surfaceMessage);
        SDL_Rect Message_rect;

        TTF_SizeText(_font, ss.str().c_str(), &width, &height);

        Message_rect.x = v.x * PIXEL_SIZE + 10 * i - 1;
        Message_rect.y = v.y * PIXEL_SIZE;
        Message_rect.w = width;
        Message_rect.h = height;
        SDL_FreeSurface(surfaceMessage);
        SDL_RenderCopy(_renderer, Message, nullptr, &Message_rect);
        SDL_DestroyTexture(Message);
    }
}

