/*
** EPITECH PROJECT, 2022
** OOP_arcade_2018
** File description:
** Created by qjean-christophe,
*/

#include "SDLUtils.hpp"

std::map<arcade::Color, SDL_Color> arcade::SDLUtils::_colorEquivalences = {
    { Color::ARCADE_TRANSPARENT,   SDL_Color{0, 0, 0, 0}           },
    { Color::ARCADE_WHITE,         SDL_Color{236, 225, 203, 255}   },
    { Color::ARCADE_BLACK,         SDL_Color{0, 43, 54, 255}         },
    { Color::ARCADE_RED,           SDL_Color{220, 50, 47, 255}       },
    { Color::ARCADE_BLUE,          SDL_Color{38, 139, 210, 255}       },
    { Color::ARCADE_GREEN,         SDL_Color{133, 153, 0, 255}       },
    { Color::ARCADE_YELLOW,        SDL_Color{228, 167, 0, 255}     },
    { Color::ARCADE_MAGENTA,       SDL_Color{211, 54, 130, 255}     },
    { Color::ARCADE_CYAN,          SDL_Color{42, 161, 152, 255}     }
};

SDL_Color arcade::SDLUtils::toSDLColor(arcade::Color c)
{
    return _colorEquivalences[c];
}