/*
** EPITECH PROJECT, 2022
** shooter-cmake2
** File description:
** Created by qjean-christophe,
*/

#ifndef SDLUTILS_HPP
#define SDLUTILS_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <sstream>
#include "../../commons/include/IGraphic.hpp"
#include "../../commons/include/Color.hpp"

namespace arcade
{
    class SDLUtils {
        private:
            static std::map<arcade::Color, SDL_Color> _colorEquivalences;

        public:
            static SDL_Color toSDLColor(arcade::Color c);
    };
}

#endif