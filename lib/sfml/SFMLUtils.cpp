/*
** EPITECH PROJECT, 2019
** SFMLUtils.cpp
** File description:
** SFMLUtils.cpp
*/

#include "SFMLUtils.hpp"

std::map<arcade::Color, sf::Color> arcade::SFMLUtils::_colorEquivalences = {
    { Color::ARCADE_TRANSPARENT,   sf::Color(0, 0, 0, 0)           },
    { Color::ARCADE_WHITE,         sf::Color(236, 225, 203, 255)   },
    { Color::ARCADE_BLACK,         sf::Color(0, 43, 54, 255)         },
    { Color::ARCADE_RED,           sf::Color(220, 50, 47, 255)       },
    { Color::ARCADE_BLUE,          sf::Color(38, 139, 210, 255)       },
    { Color::ARCADE_GREEN,         sf::Color(133, 153, 0, 255)       },
    { Color::ARCADE_YELLOW,        sf::Color(228, 167, 0, 255)     },
    { Color::ARCADE_MAGENTA,       sf::Color(211, 54, 130, 255)     },
    { Color::ARCADE_CYAN,          sf::Color(42, 161, 152, 255)     }
};

sf::Color arcade::SFMLUtils::toSFMLColor(arcade::Color c)
{
    return _colorEquivalences[c];
}

