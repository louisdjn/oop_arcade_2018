/*
** EPITECH PROJECT, 2019
** SFMLGraphics.cpp
** File description:
** SFMLGraphics.cpp
*/

#include "SFMLGraphics.hpp"

void __attribute__((constructor)) ctor()
{
    std::cout << "[Arcade] SFML graphic library loaded!" << std::endl;
}

void __attribute__((destructor)) dtor()
{
    std::cout << "[Arcade] SFML graphic library closed!" << std::endl;
}

extern "C" arcade::IGraphic *init()
{
    return new arcade::SFMLGraphics();
}

arcade::SFMLGraphics::SFMLGraphics()
{
    sf::VideoMode mode = { 1600, 900, 32 };
    this->_window = new sf::RenderWindow(mode, "Arcade - SFML Edition", sf::Style::Titlebar | sf::Style::Close);
    this->_font.loadFromFile("./assets/font.ttf");
    this->_window->setVerticalSyncEnabled(true);
}

arcade::SFMLGraphics::~SFMLGraphics()
{
    this->_window->close();
}

std::map<int, arcade::EventType> arcade::SFMLGraphics::_keyEquivalences = {
    { sf::Keyboard::A, arcade::ARCADE_KEY_A },
    { sf::Keyboard::B, arcade::ARCADE_KEY_B },
    { sf::Keyboard::C, arcade::ARCADE_KEY_C },
    { sf::Keyboard::D, arcade::ARCADE_KEY_D },
    { sf::Keyboard::E, arcade::ARCADE_KEY_E },
    { sf::Keyboard::F, arcade::ARCADE_KEY_F },
    { sf::Keyboard::G, arcade::ARCADE_KEY_G },
    { sf::Keyboard::H, arcade::ARCADE_KEY_H },
    { sf::Keyboard::I, arcade::ARCADE_KEY_I },
    { sf::Keyboard::J, arcade::ARCADE_KEY_J },
    { sf::Keyboard::K, arcade::ARCADE_KEY_K },
    { sf::Keyboard::L, arcade::ARCADE_KEY_L },
    { sf::Keyboard::M, arcade::ARCADE_KEY_M },
    { sf::Keyboard::N, arcade::ARCADE_KEY_N },
    { sf::Keyboard::O, arcade::ARCADE_KEY_O },
    { sf::Keyboard::P, arcade::ARCADE_KEY_P },
    { sf::Keyboard::Q, arcade::ARCADE_KEY_Q },
    { sf::Keyboard::R, arcade::ARCADE_KEY_R },
    { sf::Keyboard::S, arcade::ARCADE_KEY_S },
    { sf::Keyboard::T, arcade::ARCADE_KEY_T },
    { sf::Keyboard::U, arcade::ARCADE_KEY_U },
    { sf::Keyboard::V, arcade::ARCADE_KEY_V },
    { sf::Keyboard::W, arcade::ARCADE_KEY_W },
    { sf::Keyboard::X, arcade::ARCADE_KEY_X },
    { sf::Keyboard::Y, arcade::ARCADE_KEY_Y },
    { sf::Keyboard::Z, arcade::ARCADE_KEY_Z },
    { sf::Keyboard::Num0, arcade::ARCADE_KEY_0 },
    { sf::Keyboard::Num1, arcade::ARCADE_KEY_1 },
    { sf::Keyboard::Num2, arcade::ARCADE_KEY_2 },
    { sf::Keyboard::Num3, arcade::ARCADE_KEY_3 },
    { 51, arcade::ARCADE_KEY_4 },
    { sf::Keyboard::Num5, arcade::ARCADE_KEY_5 },
    { 56, arcade::ARCADE_KEY_6 },
    { sf::Keyboard::Num7, arcade::ARCADE_KEY_7 },
    { sf::Keyboard::Num8, arcade::ARCADE_KEY_8 },
    { sf::Keyboard::Num9, arcade::ARCADE_KEY_9 },
    { sf::Keyboard::F1, arcade::ARCADE_KEY_F1 },
    { sf::Keyboard::F2, arcade::ARCADE_KEY_F2 },
    { sf::Keyboard::F3, arcade::ARCADE_KEY_F3 },
    { sf::Keyboard::F4, arcade::ARCADE_KEY_F4 },
    { sf::Keyboard::F5, arcade::ARCADE_KEY_F5 },
    { sf::Keyboard::F6, arcade::ARCADE_KEY_F6 },
    { sf::Keyboard::F7, arcade::ARCADE_KEY_F7 },
    { sf::Keyboard::F8, arcade::ARCADE_KEY_F8 },
    { sf::Keyboard::F9, arcade::ARCADE_KEY_F9 },
    { sf::Keyboard::F10, arcade::ARCADE_KEY_F10 },
    { sf::Keyboard::F11, arcade::ARCADE_KEY_F11 },
    { sf::Keyboard::F12, arcade::ARCADE_KEY_F12 },
    { sf::Keyboard::Tab, arcade::ARCADE_KEY_TAB },
    { sf::Keyboard::Space, arcade::ARCADE_KEY_SPACE },
    { sf::Keyboard::Escape, arcade::ARCADE_KEY_ESCAPE },
    { sf::Keyboard::Enter, arcade::ARCADE_KEY_ENTER },
    { sf::Keyboard::BackSpace, arcade::ARCADE_KEY_DELETE },
    { sf::Keyboard::Right, arcade::ARCADE_KEY_RIGHT },
    { sf::Keyboard::Left, arcade::ARCADE_KEY_LEFT },
    { sf::Keyboard::Up, arcade::ARCADE_KEY_UP },
    { sf::Keyboard::Down, arcade::ARCADE_KEY_DOWN }
};

arcade::Event arcade::SFMLGraphics::getEvent()
{
    sf::Event event;

    if (!_window->pollEvent(event))
        return ARCADE_NO_EVENT;

    if (event.type == sf::Event::EventType::Closed)
        return ARCADE_CLOSE_WINDOW;
    if (event.type == sf::Event::EventType::KeyPressed)
        if (_keyEquivalences.find(event.key.code) != _keyEquivalences.end())
            return _keyEquivalences[event.key.code];
    if (event.type == sf::Event::EventType::MouseButtonPressed) {
        if (event.mouseButton.button == sf::Mouse::Button::Left)
            return ARCADE_CLICK_LEFT;
        else if (event.mouseButton.button == sf::Mouse::Button::Right)
            return ARCADE_CLICK_RIGHT;
    }
    return ARCADE_NO_EVENT;
}

void arcade::SFMLGraphics::clearWindow(arcade::Color color)
{
    if (this->_window)
        this->_window->clear(SFMLUtils::toSFMLColor(color));
}

void arcade::SFMLGraphics::displayWindow()
{
    if (this->_window != nullptr)
        this->_window->display();
}

void arcade::SFMLGraphics::printPixel(arcade::Vector pos, arcade::Color color)
{
    printRectangle(pos, { 1, 1 }, color);
}

void arcade::SFMLGraphics::printRectangle(arcade::Vector pos, arcade::Vector size, arcade::Color color)
{
    sf::VertexArray quad(sf::Quads, 4);

    quad[0].position = sf::Vector2f(pos.x * PIXEL_SIZE,             pos.y * PIXEL_SIZE);
    quad[1].position = sf::Vector2f((pos.x + size.x) * PIXEL_SIZE,  pos.y * PIXEL_SIZE);
    quad[2].position = sf::Vector2f((pos.x + size.x) * PIXEL_SIZE,  (pos.y + size.y) * PIXEL_SIZE);
    quad[3].position = sf::Vector2f(pos.x * PIXEL_SIZE,             (pos.y + size.y) * PIXEL_SIZE);

    for (int i = 0; i < 4; i++)
        quad[i].color = SFMLUtils::toSFMLColor(color);

    if (this->_window)
        this->_window->draw(quad);
}

void arcade::SFMLGraphics::printText(arcade::Vector v, const std::string &text, arcade::Color color,
                                     arcade::Color background)
{
    sf::Text sfText(text, this->_font, 20);

    sfText.setPosition(v.x * PIXEL_SIZE - 1, v.y * PIXEL_SIZE - 3);
    sfText.setFillColor(SFMLUtils::toSFMLColor(color));
    sfText.setOutlineColor(SFMLUtils::toSFMLColor(ARCADE_TRANSPARENT));
    sfText.setLetterSpacing(0.5);

    this->printRectangle(v, { (int) ceil(text.size() / 2.0), 1 }, background);

    if (this->_window)
        this->_window->draw(sfText);
}