/*
** EPITECH PROJECT, 2019
** SFMLUtils.hpp
** File description:
** SFMLUtils.hpp
*/

#ifndef OOP_ARCADE_2018_SFMLUTILS_HPP
#define OOP_ARCADE_2018_SFMLUTILS_HPP

#include <SFML/Graphics/Color.hpp>
#include <SFML/Window/Event.hpp>
#include <map>
#include "SFMLGraphics.hpp"

namespace arcade
{
    class SFMLUtils
    {
        private:
            static std::map<Color, sf::Color> _colorEquivalences;

        public:
            static sf::Color toSFMLColor(Color c);
    };
}

#endif //OOP_ARCADE_2018_SFMLUTILS_HPP
