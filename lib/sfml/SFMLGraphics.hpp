/*
** EPITECH PROJECT, 2019
** SFMLGraphics.hpp
** File description:
** SFMLGraphics.hpp
*/

#ifndef OOP_ARCADE_2018_SFMLGRAPHICS_HPP
#define OOP_ARCADE_2018_SFMLGRAPHICS_HPP

#include <iostream>
#include <math.h>
#include <SFML/Graphics.hpp>
#include "../../commons/include/IGraphic.hpp"
#include "SFMLUtils.hpp"

#define PIXEL_SIZE  20

namespace arcade
{
    class SFMLGraphics : public IGraphic
    {
        private:
            static std::map<int, arcade::EventType> _keyEquivalences;

            sf::RenderWindow *_window;
            sf::Font _font;

        public:
            SFMLGraphics();
            ~SFMLGraphics();

            Event getEvent() final;

            void clearWindow(Color color = ARCADE_BLACK) final;

            void displayWindow() final;

            void printPixel(Vector pos, Color color = ARCADE_WHITE) final;

            void printRectangle(Vector pos, Vector size,
                                Color color = ARCADE_WHITE) final;

            void printText(Vector v, const std::string &text,
                                   Color color = ARCADE_WHITE,
                                   Color background = ARCADE_TRANSPARENT) final;
    };
}

#endif //OOP_ARCADE_2018_SFMLGRAPHICS_HPP
