/*
** EPITECH PROJECT, 2019
** NcursesGraphics.cpp
** File description:
** NcursesGraphics.cpp
*/

#include "NcursesGraphics.hpp"

void __attribute__((constructor)) ctor()
{
    std::cout << "[Arcade] Ncurses graphic library loaded!" << std::endl;
}

void __attribute__((destructor)) dtor()
{
    std::cout << "[Arcade] Ncurses graphic library closed!" << std::endl;
}

extern "C" arcade::IGraphic *init()
{
    return new arcade::NcursesGraphics();
}

std::map<arcade::Color, short> arcade::NcursesGraphics::_colorEquivalences = {
    {ARCADE_TRANSPARENT, COLOR_BLACK},
    {ARCADE_WHITE, COLOR_WHITE},
    {ARCADE_BLACK, COLOR_BLACK},
    {ARCADE_RED, COLOR_RED},
    {ARCADE_BLUE, COLOR_BLUE},
    {ARCADE_GREEN, COLOR_GREEN},
    {ARCADE_YELLOW, COLOR_YELLOW},
    {ARCADE_MAGENTA, COLOR_MAGENTA},
    {ARCADE_CYAN, COLOR_CYAN}
};

std::map<int, arcade::EventType> arcade::NcursesGraphics::_keyEquivalences = {
    {ERR, arcade::ARCADE_NO_EVENT},
    {'a', arcade::ARCADE_KEY_A},
    {'b', arcade::ARCADE_KEY_B},
    {'c', arcade::ARCADE_KEY_C},
    {'d', arcade::ARCADE_KEY_D},
    {'e', arcade::ARCADE_KEY_E},
    {'f', arcade::ARCADE_KEY_F},
    {'g', arcade::ARCADE_KEY_G},
    {'h', arcade::ARCADE_KEY_H},
    {'i', arcade::ARCADE_KEY_I},
    {'j', arcade::ARCADE_KEY_J},
    {'k', arcade::ARCADE_KEY_K},
    {'l', arcade::ARCADE_KEY_L},
    {'m', arcade::ARCADE_KEY_M},
    {'n', arcade::ARCADE_KEY_N},
    {'o', arcade::ARCADE_KEY_O},
    {'p', arcade::ARCADE_KEY_P},
    {'q', arcade::ARCADE_KEY_Q},
    {'r', arcade::ARCADE_KEY_R},
    {'s', arcade::ARCADE_KEY_S},
    {'t', arcade::ARCADE_KEY_T},
    {'u', arcade::ARCADE_KEY_U},
    {'v', arcade::ARCADE_KEY_V},
    {'w', arcade::ARCADE_KEY_W},
    {'x', arcade::ARCADE_KEY_X},
    {'y', arcade::ARCADE_KEY_Y},
    {'z', arcade::ARCADE_KEY_Z},
    {'0', arcade::ARCADE_KEY_0},
    {'1', arcade::ARCADE_KEY_1},
    {'2', arcade::ARCADE_KEY_2},
    {'3', arcade::ARCADE_KEY_3},
    {'4', arcade::ARCADE_KEY_4},
    {'5', arcade::ARCADE_KEY_5},
    {'6', arcade::ARCADE_KEY_6},
    {'7', arcade::ARCADE_KEY_7},
    {'8', arcade::ARCADE_KEY_8},
    {'9', arcade::ARCADE_KEY_9},
    {KEY_F(1), arcade::ARCADE_KEY_F1},
    {KEY_F(2), arcade::ARCADE_KEY_F2},
    {KEY_F(3), arcade::ARCADE_KEY_F3},
    {KEY_F(4), arcade::ARCADE_KEY_F4},
    {KEY_F(5), arcade::ARCADE_KEY_F5},
    {KEY_F(6), arcade::ARCADE_KEY_F6},
    {KEY_F(7), arcade::ARCADE_KEY_F7},
    {KEY_F(8), arcade::ARCADE_KEY_F8},
    {KEY_F(9), arcade::ARCADE_KEY_F9},
    {KEY_F(10), arcade::ARCADE_KEY_F10},
    {KEY_F(11), arcade::ARCADE_KEY_F11},
    {KEY_F(12), arcade::ARCADE_KEY_F12},
    {' ', arcade::ARCADE_KEY_SPACE},
    {27, arcade::ARCADE_KEY_ESCAPE},
    {'\n', arcade::ARCADE_KEY_ENTER},
    {KEY_BACKSPACE, arcade::ARCADE_KEY_DELETE},
    {KEY_RIGHT, arcade::ARCADE_KEY_RIGHT},
    {KEY_LEFT, arcade::ARCADE_KEY_LEFT},
    {KEY_UP, arcade::ARCADE_KEY_UP},
    {KEY_DOWN, arcade::ARCADE_KEY_DOWN}
};

arcade::NcursesGraphics::NcursesGraphics() : _oneOfTwoRefresh(true)
{
    initscr();
    noecho();
    cbreak();
    timeout(0);
    keypad(stdscr, TRUE);
    curs_set(FALSE);
    start_color();
    for (int i = 1; i < 10; i++) {
        for (int j = 1; j < 10; j++) {
            init_pair((short) (i * 10 + j), _colorEquivalences[(arcade::Color) (i - 1)],
            _colorEquivalences[(arcade::Color) (j - 1)]);
        }
    }
}

arcade::NcursesGraphics::~NcursesGraphics()
{
    clear();
    endwin();
}

arcade::Event arcade::NcursesGraphics::getEvent()
{
    while (1) {
        int c = getch();
        if (_keyEquivalences.find(c) != _keyEquivalences.end())
            return (_keyEquivalences[c]);
    }
}

void arcade::NcursesGraphics::clearWindow(arcade::Color color)
{
    short clearColor = colorToNcursesColor(color);

    if (this->_oneOfTwoRefresh) {
        clear();
        bkgd(COLOR_PAIR(clearColor));
    }
}

void arcade::NcursesGraphics::displayWindow()
{
    if (this->_oneOfTwoRefresh)
        refresh();
    this->_oneOfTwoRefresh = !this->_oneOfTwoRefresh;
}

void arcade::NcursesGraphics::printPixel(arcade::Vector v, arcade::Color color)
{
    printRectangle(v, {1, 1}, color);
}

void arcade::NcursesGraphics::printRectangle(arcade::Vector pos, arcade::Vector size, arcade::Color color)
{
    if (!_oneOfTwoRefresh)
        return;

    short ncursesColor = colorToNcursesColor(color);
    attron(COLOR_PAIR(ncursesColor));
    for (int i = pos.y; i < pos.y + size.y; i++) {
        for (int j = pos.x * 2; j < (pos.x + size.x) * 2; j++) {
            mvaddch(i, j, ' ');
        }
    }
    attroff(COLOR_PAIR(ncursesColor));
}

void arcade::NcursesGraphics::printText(arcade::Vector v, const std::string &text, arcade::Color color,
                                        arcade::Color background)
{
    if (!_oneOfTwoRefresh)
        return;

    std::string newText = text;

    short ncursesColor = colorToNcursesColor(color, background);
    attron(COLOR_PAIR(ncursesColor));
    mvprintw(v.y, v.x * 2, (text + (text.size() % 2 == 0 ? "" : " ")).c_str());
    attroff(COLOR_PAIR(ncursesColor));
}

short arcade::NcursesGraphics::colorToNcursesColor(arcade::Color background)
{
    return (background + 1 + 10);
}

short arcade::NcursesGraphics::colorToNcursesColor(arcade::Color foreground, arcade::Color background)
{
    return ((foreground * 10) + background + 1 + 10);
}
