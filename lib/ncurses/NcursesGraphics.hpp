/*
** EPITECH PROJECT, 2019
** NcursesGraphics.hpp
** File description:
** NcursesGraphics.hpp
*/

#ifndef OOP_ARCADE_2018_NCURSESGRAPHICS_HPP
#define OOP_ARCADE_2018_NCURSESGRAPHICS_HPP

#include "../../commons/include/IGraphic.hpp"
#include <map>
#include <memory>
#include <iostream>
#include <curses.h>

namespace arcade {
    typedef short ncurses_color;

    class NcursesGraphics : public IGraphic {
        public:
            NcursesGraphics();
            ~NcursesGraphics() final;

            Event getEvent();
            void clearWindow(Color color = ARCADE_BLACK);
            void displayWindow();
            void printPixel(Vector v, Color color = ARCADE_WHITE);
            void printRectangle(Vector pos, Vector size,
                                        Color color = ARCADE_WHITE);
            void printText(Vector v, const std::string &text,
                                   Color color = ARCADE_WHITE,
                                   Color background = ARCADE_TRANSPARENT);

        private:
            static std::map<int, arcade::EventType> _keyEquivalences;
            static std::map<arcade::Color, short> _colorEquivalences;
            short colorToNcursesColor(arcade::Color background);
            short colorToNcursesColor(arcade::Color foreground, arcade::Color background);

            bool _oneOfTwoRefresh;
    };
}

#endif //OOP_ARCADE_2018_NCURSESGRAPHICS_HPP
