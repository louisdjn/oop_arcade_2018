/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Error.hpp
*/

#ifndef OOP_ARCADE_2018_ERROR_HPP
#define OOP_ARCADE_2018_ERROR_HPP

#include <exception>
#include <string>

namespace arcade
{
    class ArcadeException : public std::exception
    {
        public:
            explicit ArcadeException(const std::string &message);

            const char *what() const noexcept override;

        private:
            std::string _message;
    };

    class FileNotFoundException : public ArcadeException
    {
        public:
            explicit FileNotFoundException(const std::string &message);
    };

    class NoGameException : public ArcadeException
    {
        public:
            explicit NoGameException(const std::string &message);
    };

    class DynamicLinkingException : public ArcadeException
    {
        public:
            explicit DynamicLinkingException(const std::string &message);
    };
}

#endif //OOP_ARCADE_2018_ERROR_HPP
