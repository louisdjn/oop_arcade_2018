/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Core.cpp
*/

#include "Core.hpp"
#include "Menu.hpp"
#include "Leaderboard/Leaderboard.hpp"

arcade::Core::Core(std::string const &firstLib) : _graphic(nullptr), _game(nullptr),
    _graphicLib(nullptr), _gameLib(nullptr),
    _graphicsPath(std::vector<std::string>()), _currentGraphic(-1),
    _gamesPath(std::vector<std::string>()), _currentGame(ARCADE_MENU),
    _running(true)
{
    initGames();
    initGraphics(firstLib);

    loadBackgrounds();

    switchToMenu();
}

arcade::Core::~Core()
{
}

void arcade::Core::terminate()
{
    IGraphic *graphic = this->_graphic.release();
    IGame *game = this->_game.release();
    delete graphic;
    delete game;
}

void arcade::Core::handleLoop()
{
    std::chrono::high_resolution_clock::time_point lastUpdate =
        std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point lastRender =
        std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point current;

    while (this->_running) {
        current = std::chrono::high_resolution_clock::now();
        if (!this->_graphic || !this->_game)
            continue;
        arcade::Event event = this->_game->manageEvent(*this->_graphic);
        if (std::chrono::duration_cast<std::chrono::duration<double>>
            (current - lastUpdate).count() > (1.0 / 30.0)) {
            this->_game->updateGame();
            lastUpdate = current;
        }
        if (std::chrono::duration_cast<std::chrono::duration<double>>
            (current - lastRender).count() > (1.0 / 30.0)) {
            this->_graphic->clearWindow();
            drawBackground(*this->_graphic);
            this->_game->displayWindow(*this->_graphic);
            this->_graphic->displayWindow();
            lastRender = current;
        }
        manageEvent(event);
    }
}

void arcade::Core::manageEvent(arcade::Event &event)
{
    if (event == arcade::ARCADE_CLOSE_WINDOW)
        this->_running = false;
    if (event == arcade::ARCADE_KEY_ESCAPE) {
        if (this->_currentGame == ARCADE_MENU)
            this->_running = false;
        else
            switchToMenu();
    }
    if (event == arcade::ARCADE_KEY_F5) {
        int newGraphic = _currentGraphic;

        if (newGraphic - 1 < 0)
            newGraphic = static_cast<int>(_graphicsPath.size() - 1);
        else
            newGraphic -= 1;
        switchGraphics((unsigned int) newGraphic);
    }
    if (event == arcade::ARCADE_KEY_F6) {
        int newGraphic = _currentGraphic;

        if (newGraphic + 1 >= _graphicsPath.size())
            newGraphic = 0;
        else
            newGraphic += 1;
        switchGraphics((unsigned int) newGraphic);
    }
    if (event == arcade::ARCADE_KEY_F7 && this->_currentGame >= 0) {
        if (_currentGame - 1 < 0)
            _currentGame = static_cast<int>(_gamesPath.size() - 1);
        else
            _currentGame -= 1;
        switchGame((unsigned int) _currentGame);
    }
    if (event == arcade::ARCADE_KEY_F8 && this->_currentGame >= 0) {
        if (_currentGame + 1 >= _gamesPath.size())
            _currentGame = 0;
        else
            _currentGame += 1;
        switchGame((unsigned int) _currentGame);
    }
}

void arcade::Core::initGraphics(std::string const &firstLib)
{
    std::string dir = "lib";
    DIR* dirp = opendir(dir.c_str());
    struct dirent *dp;

    this->_graphicsPath.clear();
    this->_graphicsPath.push_back(firstLib);

    if (!dirp)
        throw FileNotFoundException("Directory '" + dir + "' is not reachable.");
    while ((dp = readdir(dirp)) != nullptr) {
        std::string name = dir + "/" + dp->d_name;
        if (StringUtils::endsWith(name, ".so") &&
            StringUtils::split(this->_graphicsPath[0], '/').back() != dp->d_name) {
            this->_graphicsPath.push_back(name);
        }
    }
    closedir(dirp);
    for (std::string &name : this->_graphicsPath) {
        std::cout << name << std::endl;
    }
    switchGraphics(0);
}

void arcade::Core::initGames()
{
    std::string dir = "games";
    DIR* dirp = opendir(dir.c_str());
    struct dirent *dp;

    this->_gamesPath.clear();

    if (!dirp)
        throw FileNotFoundException("Directory '" + dir + "' is not reachable.");
    while ((dp = readdir(dirp)) != nullptr) {
        if (StringUtils::endsWith(dp->d_name, ".so"))
            this->_gamesPath.push_back(dir + "/" + dp->d_name);
    }
    closedir(dirp);
}

void arcade::Core::switchGraphics(unsigned int id)
{
    char *error;

    if (id >= this->_graphicsPath.size() || id == this->_currentGraphic)
        return;
    this->_graphic = nullptr;
    if (this->_graphicLib)
        dlclose(this->_graphicLib);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    this->_graphicLib = dlopen(this->_graphicsPath[id].c_str(), RTLD_LAZY);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    auto init = reinterpret_cast<arcade::IGraphic *(*)()>(dlsym(this->_graphicLib, "init"));
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    IGraphic *graphic = _graphic.release();
    delete graphic;
    _graphic.reset(init());
    this->_currentGraphic = id;
}

void arcade::Core::switchToLeaderboard()
{
    char *error;

    this->_game = nullptr;
    if (this->_gameLib && this->_currentGame != ARCADE_LEADERBOARD)
        dlclose(this->_gameLib);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    IGame *menu = new Leaderboard(this);
    IGame *game = _game.release();
    delete game;
    _game.reset(menu);
    this->_currentGame = ARCADE_LEADERBOARD;
}

void arcade::Core::switchToMenu()
{
    char *error;

    this->_game = nullptr;
    if (this->_gameLib && this->_currentGame != ARCADE_MENU)
        dlclose(this->_gameLib);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    IGame *menu = new Menu(this);
    IGame *game = _game.release();
    delete game;
    _game.reset(menu);
    this->_currentGame = ARCADE_MENU;
}

void arcade::Core::switchGame(unsigned int id)
{
    char *error;

    if (id >= this->_gamesPath.size() || id == this->_currentGame)
        return;
    this->_game = nullptr;
    if (this->_gameLib && this->_currentGame >= 0)
        dlclose(this->_gameLib);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    this->_gameLib = dlopen(this->_gamesPath[id].c_str(), RTLD_LAZY);
    error = dlerror();
    if (error)
        throw DynamicLinkingException(std::string(error));
    auto init = reinterpret_cast<arcade::IGame *(*)()>(dlsym(this->_gameLib, "init"));
    IGame *game = _game.release();
    delete game;
    _game.reset(init());
    _game->setUsername(this->_username);
    this->_currentGame = id;
}

std::vector<std::string> const &arcade::Core::getGames() const
{
    return this->_gamesPath;
}

std::string arcade::Core::getGameName(int id)
{
    if (id < this->_gamesPath.size() && id >= 0)
        return this->_gamesPath[id].substr(17, this->_gamesPath[id].size() - 20);
    return id == - 1 ? "arcade" : "leaderboard";
}

void arcade::Core::loadBackgrounds()
{
    for (int i = -2; i < static_cast<int>(_gamesPath.size()); i++) {
        int x = 0;
        int y = 0;
        char ch;
        std::fstream fin("./assets/background_" + getGameName(i) + ".txt", std::fstream::in);
        std::vector<std::vector<arcade::Color>> colors = std::vector<std::vector<arcade::Color>>();

        if (fin.fail()) {
            fin.close();
            continue;
        }

        while (fin >> std::noskipws >> ch) {
            if (x == 0)
                colors.emplace_back(std::vector<arcade::Color>());
            if (ch == ' ') {
                x += 1;
                continue;
            }
            if (ch == '\n') {
                y += 1;
                x = 0;
            } else if (ch - '0' >= 0 && ch - '0' <= 8) {
                colors[y].emplace_back(static_cast<arcade::Color>(ch - '0'));
                x += 1;
            }
        }
        fin.close();
        this->_backgrounds.emplace(getGameName(i), colors);
    }
}

void arcade::Core::drawBackground(arcade::IGraphic &graphic)
{
    if (this->_backgrounds.find(getGameName(this->_currentGame)) == this->_backgrounds.end())
        return;
    std::vector<std::vector<arcade::Color>> colors = _backgrounds[getGameName(this->_currentGame)];

    for (int y = 0; y < colors.size(); y++)
        for (int x = 0; x < colors[y].size(); x++)
            graphic.printPixel( { x, y }, colors[y][x]);
}
