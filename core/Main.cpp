/*
** EPITECH PROJECT, 2019
** Main.cpp
** File description:
** Main.cpp
*/

#include "Core.hpp"

void printHelp()
{
    std::cout << "COUCOU C'EST LE HELP" << std::endl;
}

int main(int ac, char **av)
{
    arcade::Core *core = nullptr;
    int returnValue = SUCCESS;

    if (ac != 2) {
        printHelp();
        return (84);
    }
    try {
        core = new arcade::Core(av[1]);
        core->handleLoop();
    } catch (arcade::ArcadeException const &e) {
        std::cerr << e.what() << std::endl;
        returnValue = ERROR;
    }
    if (core)
        core->terminate();
    return returnValue;
}

