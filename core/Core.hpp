/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Core.hpp
*/

#ifndef OOP_ARCADE_2018_CORE_HPP
#define OOP_ARCADE_2018_CORE_HPP

#define SUCCESS     0
#define ERROR       84

#include <memory>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include <dlfcn.h>
#include <chrono>
#include <map>
#include "StringUtils.hpp"
#include "Error.hpp"
#include "../commons/include/IGraphic.hpp"
#include "../commons/include/IGame.hpp"
#include "../commons/include/Color.hpp"

#define ARCADE_MENU             -1
#define ARCADE_LEADERBOARD      -2

#include <fstream>

namespace arcade {
    class Core {
        private:
            static std::map<EventType, char> _keyMap;

            std::unique_ptr<arcade::IGraphic> _graphic;
            std::unique_ptr<arcade::IGame> _game;

            void *_graphicLib;
            void *_gameLib;

            std::vector<std::string> _graphicsPath;
            int _currentGraphic;
            std::vector<std::string> _gamesPath;
            int _currentGame;

            bool _running;

            std::map<std::string, std::vector<std::vector<arcade::Color>>> _backgrounds;

            void initGraphics(std::string const &firstLib);
            void initGames();
            void manageEvent(arcade::Event &event);
            void switchGraphics(unsigned int id);

            void drawBackground(IGraphic &graphic);
            void loadBackgrounds();

        public:
            Core(std::string const &firstLib);
            ~Core();

            void switchToMenu();
            void switchToLeaderboard();

            void switchGame(unsigned int id);
            void handleLoop();

            void terminate();

            std::string getGameName(int id);

            std::vector<std::string> const &getGames() const;

            std::string _username;
    };
}

#endif //OOP_ARCADE_2018_CORE_HPP
