/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Menu.hpp
*/

#ifndef OOP_ARCADE_2018_MENU_HPP
#define OOP_ARCADE_2018_MENU_HPP

#include <string>
#include "../commons/include/IGame.hpp"
#include "Core.hpp"

#define USERNAME_LENGTH     10

namespace arcade {
    class Menu : public IGame {
        private:
            Core *_handler;
            int _itemSelected;

            int _timer;

            bool _displayCursor;
        public:
            explicit Menu(Core *handler);
            ~Menu() = default;
            void setUsername(const std::string &username) final;

            Event manageEvent(IGraphic &graphic) final;
            void updateGame() final;
            void displayWindow(IGraphic &graphic) final;
    };
}

#endif //OOP_ARCADE_2018_MENU_HPP
