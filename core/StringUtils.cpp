/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** StringUtils.cpp
*/

#include "StringUtils.hpp"

bool arcade::StringUtils::endsWith(std::string const &str, std::string const &end)
{
    if (str.length() < end.length())
        return false;
    return str.compare(str.length() - end.length(), end.length(), end) == 0;
}

std::vector<std::string> arcade::StringUtils::split(std::string const &path, char d) {
    std::vector<std::string> r;
    unsigned long j = 0;

    for (unsigned long i = 0; i < path.length(); i++) {
        if (path[i] == d) {
            std::string cur = path.substr(j, i - j);
            if (cur.length())
                r.push_back(cur);
            j = i + 1;
        }
    }
    if (j < path.length())
        r.push_back(path.substr(j));
    return r;
}