/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Menu.cpp
*/

#include "Menu.hpp"

std::map<arcade::EventType, char> _keyMap = {
    { arcade::ARCADE_KEY_0, '0' },
    { arcade::ARCADE_KEY_1, '1' },
    { arcade::ARCADE_KEY_2, '2' },
    { arcade::ARCADE_KEY_3, '3' },
    { arcade::ARCADE_KEY_4, '4' },
    { arcade::ARCADE_KEY_5, '5' },
    { arcade::ARCADE_KEY_6, '6' },
    { arcade::ARCADE_KEY_7, '7' },
    { arcade::ARCADE_KEY_8, '8' },
    { arcade::ARCADE_KEY_9, '9' },
    { arcade::ARCADE_KEY_A, 'A' },
    { arcade::ARCADE_KEY_B, 'B' },
    { arcade::ARCADE_KEY_C, 'C' },
    { arcade::ARCADE_KEY_D, 'D' },
    { arcade::ARCADE_KEY_E, 'E' },
    { arcade::ARCADE_KEY_F, 'F' },
    { arcade::ARCADE_KEY_G, 'G' },
    { arcade::ARCADE_KEY_H, 'H' },
    { arcade::ARCADE_KEY_I, 'I' },
    { arcade::ARCADE_KEY_J, 'J' },
    { arcade::ARCADE_KEY_K, 'K' },
    { arcade::ARCADE_KEY_L, 'L' },
    { arcade::ARCADE_KEY_M, 'M' },
    { arcade::ARCADE_KEY_N, 'N' },
    { arcade::ARCADE_KEY_O, 'O' },
    { arcade::ARCADE_KEY_P, 'P' },
    { arcade::ARCADE_KEY_Q, 'Q' },
    { arcade::ARCADE_KEY_R, 'R' },
    { arcade::ARCADE_KEY_S, 'S' },
    { arcade::ARCADE_KEY_T, 'T' },
    { arcade::ARCADE_KEY_U, 'U' },
    { arcade::ARCADE_KEY_V, 'V' },
    { arcade::ARCADE_KEY_W, 'W' },
    { arcade::ARCADE_KEY_X, 'X' },
    { arcade::ARCADE_KEY_Y, 'Y' },
    { arcade::ARCADE_KEY_Z, 'Z' },
    { arcade::ARCADE_KEY_SPACE, ' ' }
};

arcade::Menu::Menu(Core *handler) : _handler(handler), _itemSelected(0),
    _displayCursor(true), _timer(0)
{
}

void arcade::Menu::setUsername(const std::string &username)
{
}

arcade::Event arcade::Menu::manageEvent(arcade::IGraphic &graphic)
{
    Event event = graphic.getEvent();

    std::vector<std::string> games = this->_handler->getGames();

    if (event == ARCADE_KEY_DOWN && this->_itemSelected != ARCADE_LEADERBOARD) {
        this->_itemSelected += 1;
        if (this->_itemSelected >= games.size())
            this->_itemSelected = 0;
    }
    if (event == ARCADE_KEY_UP && this->_itemSelected != ARCADE_LEADERBOARD) {
        this->_itemSelected -= 1;
        if (this->_itemSelected < 0)
            this->_itemSelected = static_cast<int>(games.size() - 1);
    }
    if (event == ARCADE_KEY_RIGHT) {
        if (this->_itemSelected == ARCADE_LEADERBOARD)
            this->_itemSelected = 0;
        else
            this->_itemSelected = ARCADE_LEADERBOARD;
    }
    if (event == ARCADE_KEY_LEFT) {
        if (this->_itemSelected == ARCADE_LEADERBOARD)
            this->_itemSelected = 0;
        else
            this->_itemSelected = ARCADE_LEADERBOARD;
    }
    if (event == ARCADE_KEY_ENTER) {
        if (this->_itemSelected == ARCADE_LEADERBOARD)
            this->_handler->switchToLeaderboard();
        else
            this->_handler->switchGame((unsigned int) _itemSelected);
    }
    if (_keyMap.find(event.getType()) != _keyMap.end()) {
        if (this->_handler->_username.size() <= USERNAME_LENGTH - 1) {
            this->_handler->_username += _keyMap[event.getType()];
        }
    }
    if (event == ARCADE_KEY_DELETE) {
        if (!this->_handler->_username.empty()) {
            this->_handler->_username = this->_handler->_username.substr(0, this->_handler->_username.size() - 1);
        }
    }
    return event;
}

void arcade::Menu::updateGame()
{
    this->_timer += 1;
    if (this->_timer % 20 == 0) {
        this->_displayCursor = !this->_displayCursor;
        this->_timer = 0;
    }
}

void arcade::Menu::displayWindow(arcade::IGraphic &graphic)
{
    std::vector<std::string> games = _handler->getGames();
    Vector gamesOrigin = { 38, 30 };
    Vector usernameOrigin = { 38, 22 };
    std::string displayName = this->_handler->_username;

    for (int i = 0; i < games.size(); i++) {
        std::string gameName = games[i].substr(17, games[i].size() - 20);
        graphic.printText({gamesOrigin.x, gamesOrigin.y + i * 2}, gameName, ARCADE_WHITE,
            i == _itemSelected ? ARCADE_MAGENTA : ARCADE_TRANSPARENT);
    }
    graphic.printText({ gamesOrigin.x + 10, gamesOrigin.y + static_cast<int>(games.size() - 1) }, "Leaderboard", ARCADE_WHITE,
                      _itemSelected == -2 ? ARCADE_CYAN : ARCADE_TRANSPARENT);
    graphic.printRectangle({ usernameOrigin.x, usernameOrigin.y }, { 5, 1 }, ARCADE_BLUE);
    if (this->_displayCursor && this->_handler->_username.size() < USERNAME_LENGTH)
            displayName.append("_");
    graphic.printText({ usernameOrigin.x, usernameOrigin.y }, displayName, ARCADE_WHITE, ARCADE_BLUE);
}
