/*
** EPITECH PROJECT, 2019
** ScoreInfo.cpp
** File description:
** ScoreInfo.cpp
*/

#include "ScoreInfo.hpp"
#include "../Error.hpp"

void arcade::ScoreInfo::loadScoreFile(std::string const &path)
{
    std::fstream file(path, std::ios::in);

    if (!_scoreList.empty())
        _scoreList.clear();
    if (file.fail())
        throw arcade::FileNotFoundException(path + " does not exist.");
    while (file.good()) {
        std::vector<std::string> parsed;
        std::string line;
        std::getline(file, line, '\n');
        parsed = split(line, ':');
        if (parsed.size() != 2)
            continue;

        _scoreList.emplace_back(parsed[0], std::atoi(parsed[1].c_str()));
    }
    file.close();
}

std::string const &arcade::ScoreInfo::getUsernameAt(std::size_t index) const
{
    if (index >= _scoreList.size())
        return (_unknownPlayer);
    return (_scoreList[index].first);
}

int arcade::ScoreInfo::getScoreAt(std::size_t index) const
{
    if (index >= _scoreList.size())
        return (-1);
    return (_scoreList[index].second);
}

std::vector<std::string> arcade::ScoreInfo::split(std::string const &path, char d) const
{
    std::vector<std::string> r;
    unsigned long j = 0;

    for (unsigned long i = 0; i < path.length(); i++) {
        if (path[i] == d) {
            std::string cur = path.substr(j, i - j);
            if (cur.length())
                r.push_back(cur);
            j = i + 1;
        }
    }
    if (j < path.length())
        r.push_back(path.substr(j));
    return r;
}