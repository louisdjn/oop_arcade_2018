/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Leaderboard.cpp
*/

#include "Leaderboard.hpp"

arcade::Leaderboard::Leaderboard(arcade::Core *handler) : _handler(handler)
{
    std::string dir = "assets";
    DIR* dirp = opendir(dir.c_str());
    struct dirent *dp;

    if (!dirp)
        throw FileNotFoundException("Directory '" + dir + "' is not reachable.");
    while ((dp = readdir(dirp)) != nullptr) {
        std::string name = dp->d_name;
        if (StringUtils::endsWith(name, ".txt") && name.rfind("score_", 0) == 0)
            _scoreFileList.push_back(dir + "/" + name);
    }
    closedir(dirp);
    if (_scoreFileList.size() > 0) {
        _currentScore.loadScoreFile(_scoreFileList[0]);
    }
}

void arcade::Leaderboard::setUsername(const std::string &username)
{
}

arcade::Event arcade::Leaderboard::manageEvent(arcade::IGraphic &graphic)
{
    Event event = graphic.getEvent();

    if (_scoreFileList.size() == 0)
        return (event);

    if (event.getType() == ARCADE_KEY_RIGHT) {
        _scoreIndex++;
    } else if (event.getType() == ARCADE_KEY_LEFT) {
        _scoreIndex--;
    }

    _scoreIndex %= _scoreFileList.size();
    (_scoreIndex < 0 ? _scoreIndex += _scoreFileList.size() : 0);
    return (event);
}

void arcade::Leaderboard::updateGame()
{
    if (_scoreFileList.size() == 0)
        return;

    if (_lastScoreIndex != _scoreIndex) {
        _lastScoreIndex = _scoreIndex;
        _currentScore.loadScoreFile(_scoreFileList[_scoreIndex]);
    }
}

void arcade::Leaderboard::displayWindow(arcade::IGraphic &graphic)
{
    Vector boardOrigin(24, 15);

    if (_scoreFileList.size() == 0) {
        graphic.printText(Vector(boardOrigin.x + 13, boardOrigin.y + 9), "No scores");
    } else {
        graphic.printText(Vector(boardOrigin.x + 13, boardOrigin.y + 9),
            "< " + getGameTitle(_scoreIndex) + " >");
        for (int i = 0; i < _currentScore.getNbScores(); i++) {
            graphic.printText(Vector(boardOrigin.x + 13, boardOrigin.y + i * 2 + 11),
                _currentScore.getUsernameAt(i));
            graphic.printText(Vector(boardOrigin.x + 19, boardOrigin.y + i * 2 + 11),
                std::to_string(_currentScore.getScoreAt(i)));
        }
    }
}

bool arcade::Leaderboard::endsWith(std::string const &str, std::string const &end) const
{
    if (str.length() < end.length())
        return false;
    return str.compare(str.length() - end.length(), end.length(), end) == 0;
}

std::string arcade::Leaderboard::getGameTitle(std::size_t index) const
{
    if (index >= _scoreFileList.size())
        return ("Unknown");

    std::string name(_scoreFileList.at(index).begin() + 13,
        _scoreFileList.at(index).end() - 4);
    if (name[0] >= 'a' && name[0] <= 'z')
        name[0] += ('A' - 'a');
    return (name);
}
