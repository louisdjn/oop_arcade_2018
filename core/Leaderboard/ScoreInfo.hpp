/*
** EPITECH PROJECT, 2019
** ScoreInfo.hpp
** File description:
** ScoreInfo.hpp
*/

#ifndef OOP_ARCADE_2018_SCOREINFO_HPP
#define OOP_ARCADE_2018_SCOREINFO_HPP

#include <vector>
#include <fstream>
#include <iostream>
#include "../../commons/include/IGame.hpp"

namespace arcade
{
    class ScoreInfo
    {
        public:
            ScoreInfo() = default;

            void loadScoreFile(std::string const &path);

            std::string const &getUsernameAt(std::size_t index) const;
            int getScoreAt(std::size_t index) const;
            std::size_t getNbScores() const { return (_scoreList.size()); }

        private:
            std::string const _unknownPlayer = "UNKNOWN";
            std::vector<std::pair<std::string, int>> _scoreList;
            std::vector<std::string> split(const std::string &path, char d) const;
    };
}

#endif //OOP_ARCADE_2018_SCOREINFO_HPP
