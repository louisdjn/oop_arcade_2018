/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Leaderboard.hpp
*/

#ifndef OOP_ARCADE_2018_LEADERBOARD_HPP
#define OOP_ARCADE_2018_LEADERBOARD_HPP

#include "../../commons/include/IGame.hpp"
#include "../Core.hpp"
#include "ScoreInfo.hpp"

namespace arcade
{
    class Leaderboard : public IGame
    {
        private:
            Core *_handler;

            int _timer;
            bool endsWith(const std::string &str, const std::string &end) const;
            std::string getGameTitle(std::size_t index) const;
            std::vector<std::string> _scoreFileList;
            std::size_t _lastScoreIndex = 0;
            std::size_t _scoreIndex = 0;
            ScoreInfo _currentScore;

        public:
            explicit Leaderboard(Core *handler);
            ~Leaderboard() = default;
            void setUsername(const std::string &username) final;

            Event manageEvent(IGraphic &graphic) final;
            void updateGame() final;
            void displayWindow(IGraphic &graphic) final;
    };
}

#endif //OOP_ARCADE_2018_LEADERBOARD_HPP
