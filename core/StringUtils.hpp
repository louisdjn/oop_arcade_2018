/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** StringUtils.hpp
*/

#ifndef OOP_ARCADE_2018_STRINGUTILS_HPP
#define OOP_ARCADE_2018_STRINGUTILS_HPP

#include <string>
#include <vector>

namespace arcade {
    class StringUtils {
        public:
            static bool endsWith(std::string const &str, std::string const &end);
            static std::vector<std::string> split(std::string const &str, char c);
    };
}

#endif //OOP_ARCADE_2018_STRINGUTILS_HPP
