/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** Error.cpp
*/

#include "Error.hpp"

arcade::ArcadeException::ArcadeException(const std::string &message)
{
    _message = message;
}

const char *arcade::ArcadeException::what() const noexcept
{
    return (&_message[0]);
}

arcade::FileNotFoundException::FileNotFoundException(const std::string &message)
    : ArcadeException(message)
{}

arcade::NoGameException::NoGameException(const std::string &message)
    : ArcadeException(message)
{}

arcade::DynamicLinkingException::DynamicLinkingException(const std::string &message)
    : ArcadeException(message)
{}