/*
** EPITECH PROJECT, 2019
** Nibbler.hpp
** File description:
** Nibbler.hpp
*/

#ifndef OOP_ARCADE_2018_NIBBLER_HPP
#define OOP_ARCADE_2018_NIBBLER_HPP

#include "../../commons/include/IGame.hpp"
#include <memory>
#include <iostream>
#include <vector>
#include <random>
#include <cstdlib>
#include <sstream>
#include <algorithm>

namespace arcade {
    class Nibbler : public IGame {
        public:
            Nibbler();

            void setUsername(const std::string &username) final;
            Event manageEvent(IGraphic &graphic) final;
            void updateGame() final;
            void displayWindow(IGraphic &graphic) final;

            typedef enum
            {
                UP = 1,
                LEFT = 2,
                DOWN = 3,
                RIGHT = 4
            } Direction;

            typedef enum
            {
                EMPTY,
                SNAKE,
                FRUIT,
                OUT
            } CellType;

        private:
            std::string _username;
            std::vector<Vector> _snakeCells;
            Vector _fruitPos;
            int _roomWidth;
            int _roomHeight;
            Direction _currentDirection;
            Direction _lastDirection;
            int _clock;
            int _score;

            bool _endMenu;
            int _endMenuSelection;

            bool _closeRequest;
            bool _win;
            int _lastScore;

            Vector directionToVector(Direction direction);
            CellType getTypeOfCell(Vector const &pos);
            Direction getDirectionFromInput(Event const &event);
            void changeFruitPosition();
            int getNbFreeCells();
            void restartGame();
            void handleDisplayEndMenu(arcade::IGraphic &graphic);
            void endGame(bool win);

            bool areOppositeDirections(Direction direction1, Direction direction2);
            std::string directionToString(Direction direction);
            void saveScore() const;
            std::vector<std::string> split(const std::string &path, char d) const;
    };
}

#endif //OOP_ARCADE_2018_NIBBLER_HPP
