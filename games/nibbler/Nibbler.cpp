/*
** EPITECH PROJECT, 2019
** Nibbler.cpp
** File description:
** Nibbler.cpp
*/

#include <fstream>
#include <cstring>
#include "Nibbler.hpp"

void __attribute__((constructor)) ctor()
{
    std::cout << "[Arcade] Nibbler game loaded!" << std::endl;
}

void __attribute__((destructor)) dtor()
{
    std::cout << "[Arcade] Nibbler game closed!" << std::endl;
}

extern "C" arcade::IGame *init()
{
    return new arcade::Nibbler();
}

arcade::Nibbler::Nibbler()
    : _roomWidth(30), _roomHeight(20), _currentDirection(RIGHT),
      _lastDirection(RIGHT), _clock(0), _closeRequest(false), _endMenu(false),
      _endMenuSelection(0), _win(false)
{
    srand(static_cast<unsigned int>(time(nullptr)));
    restartGame();
}

void arcade::Nibbler::setUsername(const std::string &username)
{
    _username = username;
}

arcade::Event arcade::Nibbler::manageEvent(arcade::IGraphic &graphic)
{
    Event event = graphic.getEvent();

    if (!_endMenu)
        _currentDirection = getDirectionFromInput(event);
    else {
        if (event == ARCADE_KEY_UP || event == ARCADE_KEY_DOWN) {
            _endMenuSelection += 1;
            _endMenuSelection %= 2;
        }
        if (event == ARCADE_KEY_ENTER) {
            if (_endMenuSelection)
                this->_closeRequest = true;
            else {
                this->_endMenu = false;
                this->_win = false;
            }
        }
    }

    return _closeRequest ? arcade::Event(ARCADE_KEY_ESCAPE) : event;
}

void arcade::Nibbler::endGame(bool win)
{
    this->_endMenu = true;
    this->_win = win;
    this->_lastScore = this->_score;
    std::cout << "end" << std::endl;
    saveScore();
    restartGame();
}

void arcade::Nibbler::handleDisplayEndMenu(arcade::IGraphic &graphic)
{
    arcade::Vector origin = { 34, 18 };

    graphic.printRectangle( { origin.x + 2, origin.y }, { 8, 1 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 1, origin.y + 1 }, { 1, 3 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 2 + 6 + 2, origin.y + 1 }, { 1, 3 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 2, origin.y + 4 }, { 8, 1 }, this->_win ? ARCADE_GREEN : ARCADE_RED);

    if (this->_win)
        graphic.printText( { origin.x + 3, origin.y + 2 }, "YOU WIN !!!!");
    else
        graphic.printText( { origin.x + 3, origin.y + 2 }, "YOU LOSE ...");

    graphic.printText( { origin.x, origin.y + 7 }, "Name :", ARCADE_WHITE, ARCADE_CYAN);
    graphic.printText( { origin.x + 8, origin.y + 7 }, "Score :", ARCADE_WHITE, ARCADE_CYAN);

    std::stringstream ss;
    ss << this->_lastScore;
    std::string score = ss.str();

    graphic.printText( { origin.x, origin.y + 9 }, this->_username.empty() ? "NONE" : this->_username);
    graphic.printText( { origin.x + 8, origin.y + 9 }, score);

    graphic.printText( { origin.x + 3, origin.y + 12 }, "Restart game", ARCADE_WHITE,
        !_endMenuSelection ? ARCADE_MAGENTA : ARCADE_TRANSPARENT);
    graphic.printText( { origin.x + 3, origin.y + 14 }, "Back to menu",
        ARCADE_WHITE, _endMenuSelection ? ARCADE_MAGENTA : ARCADE_TRANSPARENT);
}

void arcade::Nibbler::updateGame()
{
    if (_endMenu)
        return;
    _clock++;
    if (_clock == 5) {
        Vector newPos = Vector(_snakeCells.front().x + directionToVector(_currentDirection).x,
                               _snakeCells.front().y + directionToVector(_currentDirection).y);
        CellType cellType = getTypeOfCell(newPos);

        _lastDirection = _currentDirection;
        if (cellType == SNAKE || cellType == OUT) {
            endGame(false);
        } else if (cellType == FRUIT) {
            _snakeCells.insert(_snakeCells.begin(), newPos);
            _score++;
            if (getNbFreeCells() != 0)
                changeFruitPosition();
            else
                endGame(true);
        } else if (cellType == EMPTY) {
            _snakeCells.insert(_snakeCells.begin(), newPos);
            _snakeCells.pop_back();
        }
        _clock = 0;
    }
}

void arcade::Nibbler::displayWindow(arcade::IGraphic &graphic)
{
    Vector mapOrigin(24, 15);

    graphic.printRectangle(Vector(mapOrigin.x + 1, mapOrigin.y), Vector(_roomWidth, 1), ARCADE_WHITE);
    graphic.printRectangle(Vector(mapOrigin.x, mapOrigin.y + 1), Vector(1, _roomHeight), ARCADE_WHITE);
    graphic.printRectangle(Vector(mapOrigin.x + _roomWidth + 1, mapOrigin.y + 1), Vector(1, _roomHeight), ARCADE_WHITE);
    graphic.printRectangle(Vector(mapOrigin.x + 1, mapOrigin.y +  _roomHeight + 1), Vector(_roomWidth, 1), ARCADE_WHITE);

    if (_endMenu) {
        handleDisplayEndMenu(graphic);
        return;
    }
    for (int i = 0; i < _snakeCells.size(); i++) {
        Vector v = _snakeCells.at(i);
        if (i == 0) {
            graphic.printText(Vector(v.x + 1 + mapOrigin.x, v.y + 1 + mapOrigin.y), directionToString(_currentDirection), ARCADE_WHITE, ARCADE_GREEN);
        } else {
            graphic.printPixel(Vector(v.x + 1 + mapOrigin.x, v.y + 1 + mapOrigin.y), ARCADE_GREEN);
        }
    }
    graphic.printText(Vector(_fruitPos.x + 1 + mapOrigin.x, _fruitPos.y + 1 + mapOrigin.y), "()", ARCADE_WHITE, ARCADE_RED);
    graphic.printText(Vector(0, 0), _username);
    graphic.printText(Vector(0, 2), std::string("Score: ") + std::to_string(_score), ARCADE_RED, ARCADE_YELLOW);
}

arcade::Nibbler::Direction arcade::Nibbler::getDirectionFromInput(const arcade::Event &event)
{
    Direction direction = _currentDirection;

    switch (event.getType()) {
        case ARCADE_KEY_UP:
            direction = UP;
            break;
        case ARCADE_KEY_RIGHT:
            direction = RIGHT;
            break;
        case ARCADE_KEY_LEFT:
            direction = LEFT;
            break;
        case ARCADE_KEY_DOWN:
            direction = DOWN;
            break;
        default:
            break;
    }
    if (direction % 2 == _lastDirection  % 2 && direction != _lastDirection)
        direction = _currentDirection;
    return (direction);
}

arcade::Vector arcade::Nibbler::directionToVector(arcade::Nibbler::Direction direction)
{
    Vector vector(0, 0);

    switch (direction) {
        case UP:
            vector.y = -1;
            break;
        case DOWN:
            vector.y = 1;
            break;
        case LEFT:
            vector.x = -1;
            break;
        case RIGHT:
            vector.x = 1;
            break;
        default:
            break;
    };
    return (vector);
}

arcade::Nibbler::CellType arcade::Nibbler::getTypeOfCell(const arcade::Vector &pos)
{
    if (pos.x < 0 || pos.x >= _roomWidth || pos.y < 0 || pos.y >= _roomHeight)
        return (OUT);
    for (Vector &v : _snakeCells) {
        if (pos == v)
            return (SNAKE);
    }
    if (pos == _fruitPos)
        return (FRUIT);
    return (EMPTY);
}

void arcade::Nibbler::restartGame()
{
    if (!_snakeCells.empty())
        _snakeCells.clear();
    _snakeCells.emplace_back(_roomWidth / 2, _roomHeight / 2);
    _snakeCells.emplace_back(_roomWidth / 2 - 1, _roomHeight / 2);
    _snakeCells.emplace_back(_roomWidth / 2 - 2, _roomHeight / 2);
    _snakeCells.emplace_back(_roomWidth / 2 - 3, _roomHeight / 2);
    _currentDirection = RIGHT;
    _clock = 0;
    _score = 0;
    changeFruitPosition();
}

void arcade::Nibbler::changeFruitPosition()
{
    int pos = rand() / (RAND_MAX / (getNbFreeCells() + 1) + 1)
              % (getNbFreeCells() + 1);
    int curr_pos = 0;

    for (int x = 0; x < _roomWidth && curr_pos != pos; x++) {
        for (int y = 0; y < _roomHeight && curr_pos != pos; y++) {
            if (getTypeOfCell(Vector(x, y)) != SNAKE)
                curr_pos++;
            if (curr_pos == pos)
                _fruitPos = Vector(x, y);
        }
    }
}

int arcade::Nibbler::getNbFreeCells()
{
    int nbFreeCells = static_cast<int>(_roomHeight * _roomWidth
                                       - _snakeCells.size());
    return (nbFreeCells);
}

bool arcade::Nibbler::areOppositeDirections(arcade::Nibbler::Direction direction1,
                                            arcade::Nibbler::Direction direction2)
{
    return abs(direction1 - direction2) == 2;
}

std::string arcade::Nibbler::directionToString(arcade::Nibbler::Direction direction)
{
    switch (direction) {
        case LEFT:
            return ("< ");
        case RIGHT:
            return (" >");
        case UP:
            return ("/\\");
        case DOWN:
            return ("\\/");
        default:
            return ("");
    }
}

void arcade::Nibbler::saveScore() const
{
    std::fstream scoreFile("assets/score_nibbler.txt", std::ios::in);
    std::vector<std::pair<std::string, int>> scoreList;

    if (!scoreFile.fail()) {
        while (scoreFile.good()) {
            std::vector<std::string> parsed;
            std::string line;
            std::getline(scoreFile, line, '\n');
            parsed = split(line, ':');
            if (parsed.size() != 2)
                continue;
            scoreList.emplace_back(parsed[0], std::atoi(parsed[1].c_str()));
        }
    }
    scoreList.emplace_back((_username != "" ? _username : "PLAYER"), _score);
    std::sort(scoreList.begin(), scoreList.end(),
        [](std::pair<std::string, int> const &a,
           std::pair<std::string, int> const &b) { return (a.second > b.second); });

    if (scoreList.size() > 5) {
        scoreList.erase(scoreList.begin() + 5, scoreList.end());
    }
    scoreFile.close();
    scoreFile.open("./assets/score_nibbler.txt",
        std::ios::out | std::ios::trunc);
    if (scoreFile.fail())
        return;
    for (std::pair<std::string, int> const &pair : scoreList) {
        std::string line = pair.first + ":" + std::to_string(pair.second)
                           + "\n";
        scoreFile.write(line.c_str(), line.size());
    }
    scoreFile.close();
}

std::vector<std::string> arcade::Nibbler::split(std::string const &path, char d) const
{
    std::vector<std::string> r;
    unsigned long j = 0;

    for (unsigned long i = 0; i < path.length(); i++) {
        if (path[i] == d) {
            std::string cur = path.substr(j, i - j);
            if (cur.length())
                r.push_back(cur);
            j = i + 1;
        }
    }
    if (j < path.length())
        r.push_back(path.substr(j));
    return r;
}