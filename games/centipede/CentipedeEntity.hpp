/*
** EPITECH PROJECT, 2019
** CentipedeEntity.hpp
** File description:
** CentipedeEntity.hpp
*/

#ifndef OOP_ARCADE_2018_CENTIPEDEENTITY_HPP
#define OOP_ARCADE_2018_CENTIPEDEENTITY_HPP

#include "../../commons/include/Vector.hpp"
#include "CellType.hpp"
#include "Direction.hpp"
#include <cstddef>
#include <vector>

namespace arcade
{
    namespace CentipedeGame
    {
        class CentipedeEntity
        {
        public:

            CentipedeEntity(Vector const &pos, std::size_t length, arcade::CentipedeGame::Direction direction);
            CentipedeEntity(std::vector<Vector> const &body, arcade::CentipedeGame::Direction direction);

            std::size_t getLength() const { return (_body.size()); }

            Vector const getPosPart(std::size_t index) const;
            std::vector<Vector> const &getBody() const { return (_body); }
            Vector getPosInFront() const;
            CentipedeEntity splitAtIndex(std::size_t index);
            void move(CellType cellInFront = EMPTY);

        private:
            std::vector<Vector> _body;
            arcade::CentipedeGame::Direction _currentDirection;

            static Vector addDirectionToVector(Vector const &pos, Direction direction);
        };
    }
}

#endif //OOP_ARCADE_2018_CENTIPEDEENTITY_HPP
