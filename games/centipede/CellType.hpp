/*
** EPITECH PROJECT, 2019
** CellType.hpp
** File description:
** CellType.hpp
*/

#ifndef OOP_ARCADE_2018_CELLTYPE_HPP
#define OOP_ARCADE_2018_CELLTYPE_HPP

namespace arcade
{
    namespace CentipedeGame
    {
        typedef enum {
            EMPTY,
            OBSTACLE
        } CellType;
    }
}

#endif //OOP_ARCADE_2018_CELLTYPE_HPP