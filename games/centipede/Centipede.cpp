/*
** EPITECH PROJECT, 2019
** Centipede.cpp
** File description:
** Centipede.cpp
*/

#include "Centipede.hpp"

void __attribute__((constructor)) ctor()
{
    std::cout << "[Arcade] Centipede game loaded!" << std::endl;
}

void __attribute__((destructor)) dtor()
{
    std::cout << "[Arcade] Centipede game closed!" << std::endl;
}

extern "C" arcade::IGame *init()
{
    return new arcade::CentipedeGame::Centipede();
}

arcade::CentipedeGame::Centipede::Centipede()
    : _roomHeight(20), _roomWidth(30), _endMenu(false), _endMenuSelection(0),
    _closeRequest(false), _win(false), _lastScore(0)
{
    setSeedFromTime();
    restartGame();
}

void arcade::CentipedeGame::Centipede::setUsername(const std::string &username)
{
    _username = username;
}

arcade::Event arcade::CentipedeGame::Centipede::manageEvent(arcade::IGraphic &graphic)
{
    Event event = graphic.getEvent();

    if (!_endMenu) {
        switch (event.getType()) {
            case ARCADE_KEY_UP:
                _currentDirectionPlayer = UP;
                break;
            case ARCADE_KEY_DOWN:
                _currentDirectionPlayer = DOWN;
                break;
            case ARCADE_KEY_LEFT:
                _currentDirectionPlayer = LEFT;
                break;
            case ARCADE_KEY_RIGHT:
                _currentDirectionPlayer = RIGHT;
                break;
            case ARCADE_KEY_SPACE:
                if (_bulletList.empty())
                    _bulletList.emplace_back(_playerPos.x, _playerPos.y - 1);
                break;
            default:
                break;
        }
    } else {
        if (event == ARCADE_KEY_UP || event == ARCADE_KEY_DOWN) {
            _endMenuSelection += 1;
            _endMenuSelection %= 2;
        }
        if (event == ARCADE_KEY_ENTER) {
            if (_endMenuSelection)
                this->_closeRequest = true;
            else
                this->_endMenu = false;
        }
    }
    return (_closeRequest ? arcade::Event(ARCADE_KEY_ESCAPE) : event);
}

void arcade::CentipedeGame::Centipede::endGame(bool win)
{
    this->_endMenu = true;
    this->_win = win;
    this->_lastScore = _score;
    restartGame();
}

void arcade::CentipedeGame::Centipede::handleDisplayEndMenu(arcade::IGraphic &graphic)
{
    arcade::Vector origin = { 34, 18 };

    graphic.printRectangle( { origin.x + 2, origin.y }, { 8, 1 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 1, origin.y + 1 }, { 1, 3 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 2 + 6 + 2, origin.y + 1 }, { 1, 3 }, this->_win ? ARCADE_GREEN : ARCADE_RED);
    graphic.printRectangle( { origin.x + 2, origin.y + 4 }, { 8, 1 }, this->_win ? ARCADE_GREEN : ARCADE_RED);

    if (this->_win)
        graphic.printText( { origin.x + 3, origin.y + 2 }, "YOU WIN !!!!");
    else
        graphic.printText( { origin.x + 3, origin.y + 2 }, "YOU LOSE ...");

    graphic.printText( { origin.x, origin.y + 7 }, "Name :", ARCADE_WHITE, ARCADE_CYAN);
    graphic.printText( { origin.x + 8, origin.y + 7 }, "Score :", ARCADE_WHITE, ARCADE_CYAN);

    std::stringstream ss;
    ss << this->_lastScore;
    std::string score = ss.str();

    graphic.printText( { origin.x, origin.y + 9 }, this->_username.empty() ? "NONE" : this->_username);
    graphic.printText( { origin.x + 8, origin.y + 9 }, score);

    graphic.printText( { origin.x + 3, origin.y + 12 }, "Restart game", ARCADE_WHITE,
                       !_endMenuSelection ? ARCADE_MAGENTA : ARCADE_TRANSPARENT);
    graphic.printText( { origin.x + 3, origin.y + 14 }, "Back to menu",
                       ARCADE_WHITE, _endMenuSelection ? ARCADE_MAGENTA : ARCADE_TRANSPARENT);
}


void arcade::CentipedeGame::Centipede::updateGame()
{
    if (_endMenu)
        return;
    if (gameIsLost() || _centipedeCount > 20) {
        saveScore();
        endGame(_centipedeCount > 20);
    }
    if (_centipedeList.size() == 0) {
        _centipedeList.emplace_back(Vector(rand_r(&_seed) % _roomWidth, -1),
            15, ((rand_r(&_seed) % 2) == 0 ? LEFT : RIGHT));
        _centipedeCount++;
    }
    if (_centipedeClock == getCentipedeClockCycle()) {
        _centipedeClock = 0;
        for (CentipedeEntity &centipede : _centipedeList) {
            Vector v = centipede.getPosPart(0);
            centipede.move(getCellType(centipede.getPosInFront()));
            v = centipede.getPosPart(0);
        }
    }
    if (_playerClock == 2) {
        _playerClock = 0;
        movePlayer();
    }
    for (size_t i = 0; i < _bulletList.size(); i++) {
        bool bulletDestroyed = false;
        if (_bulletList.at(i).y < 0) {
            _bulletList.erase(_bulletList.begin() + i);
            break;
        }
        for (size_t j = 0; j < _boxList.size(); j++) {
            if (_bulletList.at(i) == _boxList.at(j).getPos()) {
                _boxList.at(j).decreaseHealth(1);
                _bulletList.erase(_bulletList.begin() + i);
                bulletDestroyed = true;
                break;
            }
        }
        if (!bulletDestroyed && _bulletList.size() > 0) {
            for (std::size_t listSize = 0; listSize < _centipedeList.size() &&
                !bulletDestroyed; listSize++) {
                for (std::size_t j = 0; j <
                    _centipedeList.at(listSize).getLength(); j++) {
                    if (_bulletList.at(i) ==
                        _centipedeList.at(listSize).getPosPart(j)) {
                        _boxList.emplace_back(Vector(_bulletList.at(i).x,
                            _bulletList.at(i).y));
                        _centipedeList.push_back(
                            _centipedeList.at(listSize).splitAtIndex(j));
                        _bulletList.erase(_bulletList.begin() + i);
                        bulletDestroyed = true;
                        _score++;
                        break;
                    }
                }
            }
        }
        if (!bulletDestroyed && getCellType(_bulletList.at(i)) == OBSTACLE) {
            _bulletList.erase(_bulletList.begin() + i);
        }
    }
    if (_bulletClock == 1) {
        _bulletClock = 0;
        for (Vector &v : _bulletList)
            v.y--;
    }
    cleanLists();
    _bulletClock++;
    _centipedeClock++;
    _playerClock++;
}

void arcade::CentipedeGame::Centipede::displayWindow(arcade::IGraphic &graphic)
{
    Vector mapOrigin(24, 15);
    Vector scoreOrigin(0, 0);

    graphic.printRectangle(Vector(mapOrigin.x + 1, mapOrigin.y),
        Vector(_roomWidth, 1));
    graphic.printRectangle(Vector(mapOrigin.x, mapOrigin.y + 1), Vector(1,
        _roomHeight));
    graphic.printRectangle(Vector(mapOrigin.x + _roomWidth + 1,
        mapOrigin.y + 1), Vector(1, _roomHeight));
    graphic.printRectangle(Vector(mapOrigin.x + 1, mapOrigin.y +
        _roomHeight + 1), Vector(_roomWidth, 1));

    if (_endMenu) {
        handleDisplayEndMenu(graphic);
        return;
    }

    graphic.printText({ scoreOrigin.x, scoreOrigin.y }, _username);
    graphic.printText({ scoreOrigin.x, scoreOrigin.y + 2 }, "Score: " + std::to_string(_score));

    for (Box box : _boxList) {
        Color color;
        switch (box.getHealth()) {
            case 3:
                color = ARCADE_BLUE;
                break;
            case 2:
                color = ARCADE_BLUE;
                break;
            case 1:
                color = ARCADE_RED;
                break;
            default:
                color = ARCADE_BLUE;
                break;
        }
        Vector v = box.getPos();
        graphic.printPixel(Vector(v.x + mapOrigin.x + 1, v.y + mapOrigin.y + 1), color);
    }
    for (CentipedeEntity const &centipede : _centipedeList) {
        for (Vector const &v : centipede.getBody()) {
            if (v.x >= 0 && v.x < _roomWidth && v.y >= 0 && v.y < _roomHeight)
                graphic.printPixel(Vector(v.x + mapOrigin.x + 1, v.y + mapOrigin.y + 1), ARCADE_GREEN);
        }
    }
    for (Vector const &v : _bulletList)
        if (v.y >= 0)
            graphic.printPixel(Vector(v.x + mapOrigin.x + 1, v.y + mapOrigin.y + 1), ARCADE_MAGENTA);
    graphic.printPixel(Vector(_playerPos.x + mapOrigin.x + 1, _playerPos.y + mapOrigin.y + 1), ARCADE_CYAN);
}

arcade::CentipedeGame::CellType arcade::CentipedeGame::Centipede::getCellType(const arcade::Vector &pos) const
{
    if (pos.x < 0 || pos.x >= _roomWidth || pos.y < 0 || pos.y >= _roomHeight)
        return (OBSTACLE);

    for (Box const &box : _boxList)
        if (box.getPos() == pos)
            return (OBSTACLE);

    for (CentipedeEntity const &centipede : _centipedeList)
        for (Vector const &v : centipede.getBody())
            if (v == pos)
                return (OBSTACLE);
    return (EMPTY);
}

void arcade::CentipedeGame::Centipede::movePlayer()
{
    switch (_currentDirectionPlayer) {
        case LEFT:
            if (_playerPos.x > 0)
                _playerPos.x--;
            break;
        case RIGHT:
            if (_playerPos.x < _roomWidth - 1)
                _playerPos.x++;
            break;
        case UP:
            if (_playerPos.y > _roomHeight * 0.8)
                _playerPos.y--;
            break;
        case DOWN:
            if (_playerPos.y < _roomHeight - 1)
                _playerPos.y++;
            break;
        default:
            break;
    }
    _currentDirectionPlayer = NONE;
}

void arcade::CentipedeGame::Centipede::cleanLists()
{
    for (std::size_t i = 0; i < _centipedeList.size(); i++) {
        if (_centipedeList.at(i).getLength() == 0) {
            _centipedeList.erase(_centipedeList.begin() + i);
        } else if (_centipedeList.at(i).getPosPart(0).y == _roomHeight) {
            _centipedeList.erase(_centipedeList.begin() + i);
            _score--;
        }
    }
    for (std::size_t i = 0; i < _boxList.size(); i++) {
        if (_boxList.at(i).getHealth() <= 0) {
            _boxList.erase(_boxList.begin() + i);
        }
    }
}

void arcade::CentipedeGame::Centipede::generateBoxes(std::size_t nbBoxes)
{
    int freeCells = static_cast<int>(_roomWidth * (_roomHeight * 0.8));

    _boxList.clear();
    srand(static_cast<unsigned int>(time(NULL)));
    for (std::size_t i = 0; i < nbBoxes; i++) {
        std::size_t value = rand_r(&_seed) % (freeCells + 1);
        _boxList.emplace_back(Vector(value % _roomWidth,
            value / _roomWidth));
        freeCells--;
    }
}

void arcade::CentipedeGame::Centipede::restartGame()
{
    if (!_centipedeList.empty())
        _centipedeList.clear();
    if (!_boxList.empty())
        _boxList.clear();
    _centipedeList.emplace_back(Vector(rand_r(&_seed) % _roomWidth, -1),
        15, ((rand_r(&_seed) % 2) == 0 ? LEFT : RIGHT));
    _centipedeCount = 1;
    generateBoxes((_roomWidth * (_roomHeight / 2)) / 10);
    _playerPos.x = _roomWidth / 2;
    _playerPos.y = _roomHeight - 1;
    _bulletClock = 0;
    _playerClock = 0;
    _centipedeClock = 0;
    _score = 0;
}

bool arcade::CentipedeGame::Centipede::gameIsLost()
{
    for (CentipedeEntity const &centipede : _centipedeList)
        for (Vector const &v : centipede.getBody())
            if (v.x == _playerPos.x && v.y == _playerPos.y)
                return (true);
    return (false);
}

int arcade::CentipedeGame::Centipede::getCentipedeClockCycle()
{
    if (_centipedeCount <= 2)
        return (10);
    else if (_centipedeCount > 2 && _centipedeCount <= 10)
        return (8);
    else if (_centipedeCount > 10 && _centipedeCount <= 15)
        return (6);
    else
        return (4);
}

void arcade::CentipedeGame::Centipede::setSeedFromTime()
{
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    _seed = static_cast<unsigned int>(spec.tv_nsec);
}

void arcade::CentipedeGame::Centipede::saveScore() const
{
    std::fstream scoreFile("assets/score_centipede.txt", std::ios::in);
    std::vector<std::pair<std::string, int>> scoreList;

    if (!scoreFile.fail()) {
        while (scoreFile.good()) {
            std::vector<std::string> parsed;
            std::string line;
            std::getline(scoreFile, line, '\n');
            parsed = split(line, ':');
            if (parsed.size() != 2)
                continue;
            scoreList.emplace_back(parsed[0], std::atoi(parsed[1].c_str()));
        }
    }
    scoreList.emplace_back((_username != "" ? _username : "PLAYER"), _score);
    std::sort(scoreList.begin(), scoreList.end(),
        [](std::pair<std::string, int> const &a,
           std::pair<std::string, int> const &b) { return (a.second > b.second); });

    if (scoreList.size() > 5) {
        scoreList.erase(scoreList.begin() + 5, scoreList.end());
    }
    scoreFile.close();
    scoreFile.open("./assets/score_centipede.txt",
        std::ios::out | std::ios::trunc);
    if (scoreFile.fail())
        return;
    for (std::pair<std::string, int> const &pair : scoreList) {
        std::string line = pair.first + ":" + std::to_string(pair.second)
                           + "\n";
        scoreFile.write(line.c_str(), line.size());
    }
    scoreFile.close();
}

std::vector<std::string> arcade::CentipedeGame::Centipede::split(std::string const &path, char d) const
{
    std::vector<std::string> r;
    unsigned long j = 0;

    for (unsigned long i = 0; i < path.length(); i++) {
        if (path[i] == d) {
            std::string cur = path.substr(j, i - j);
            if (cur.length())
                r.push_back(cur);
            j = i + 1;
        }
    }
    if (j < path.length())
        r.push_back(path.substr(j));
    return r;
}
