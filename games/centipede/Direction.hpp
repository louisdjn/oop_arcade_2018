/*
** EPITECH PROJECT, 2019
** Direction.hpp
** File description:
** Direction.hpp
*/

#ifndef OOP_ARCADE_2018_DIRECTION_HPP
#define OOP_ARCADE_2018_DIRECTION_HPP

namespace arcade
{
    namespace CentipedeGame
    {
        typedef enum
        {
            NONE,
            UP,
            LEFT,
            DOWN,
            RIGHT
        } Direction;
    }
}

#endif //OOP_ARCADE_2018_DIRECTION_HPP