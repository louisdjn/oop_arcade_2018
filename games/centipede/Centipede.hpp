/*
** EPITECH PROJECT, 2019
** Centipede.hpp
** File description:
** Centipede.hpp
*/

#ifndef OOP_ARCADE_2018_CENTIPEDE_HPP
#define OOP_ARCADE_2018_CENTIPEDE_HPP

#include "../../commons/include/IGame.hpp"
#include "CellType.hpp"
#include "Box.hpp"
#include "CentipedeEntity.hpp"
#include "Direction.hpp"
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <sstream>
#include <algorithm>

namespace arcade {
    namespace CentipedeGame {
        class Centipede : public IGame {
            public:
                Centipede();

                void setUsername(const std::string &username);

                Event manageEvent(IGraphic &graphic);
                void updateGame();
                void displayWindow(IGraphic &graphic);

            private:
                std::string _username = "";
                int _roomHeight;
                int _roomWidth;
                Vector _playerPos;
                int _score = 0;
                int _bulletClock = 0;
                int _playerClock = 0;
                int _centipedeClock = 0;
                arcade::CentipedeGame::Direction _currentDirectionPlayer = NONE;
                std::vector<Box> _boxList;
                std::vector<CentipedeEntity> _centipedeList;
                std::vector<Vector> _bulletList;
                int _centipedeCount = 0;
                unsigned int _seed;

                bool _endMenu;
                int _endMenuSelection;

                bool _closeRequest;
                bool _win;
                int _lastScore;

                void endGame(bool win);
                void handleDisplayEndMenu(arcade::IGraphic &graphic);

                arcade::CentipedeGame::CellType getCellType(Vector const &pos) const;
                void movePlayer();
                void cleanLists();
                void restartGame();
                void generateBoxes(std::size_t nbBoxes);
                bool gameIsLost();
                int getCentipedeClockCycle();
                void setSeedFromTime();
                void saveScore() const;
                std::vector<std::string> split(const std::string &path, char d) const;
        };
    }
}

#endif //OOP_ARCADE_2018_CENTIPEDE_HPP
