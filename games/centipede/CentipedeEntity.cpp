/*
** EPITECH PROJECT, 2019
** CentipedeEntity.cpp
** File description:
** CentipedeEntity.cpp
*/

#include <iostream>
#include "CentipedeEntity.hpp"

arcade::CentipedeGame::CentipedeEntity::CentipedeEntity(arcade::Vector const &pos, std::size_t length,
                                                    arcade::CentipedeGame::Direction direction)
    : _currentDirection(direction)
{
    int dirMutliplier = (_currentDirection == LEFT ? 1 : -1);
    for (std::size_t i = 0; i < length; i++) {
        _body.emplace_back(Vector(pos.x + (i * dirMutliplier), pos.y));
    }
}

arcade::CentipedeGame::CentipedeEntity::CentipedeEntity(std::vector<arcade::Vector> const &body, Direction direction)
    : _currentDirection(direction)
{
    _body = body;
}

const arcade::Vector arcade::CentipedeGame::CentipedeEntity::getPosPart(std::size_t index) const
{
    if (index >= _body.size())
        return (Vector(0, 0));
    return (_body.at(index));
}

arcade::CentipedeGame::CentipedeEntity arcade::CentipedeGame::CentipedeEntity::splitAtIndex(std::size_t index)
{
    if (index >= _body.size() || _body.size() == 0)
        return (arcade::CentipedeGame::CentipedeEntity(arcade::Vector(0, 0), 0, UP));

    std::vector<Vector> newBodyTail(_body.begin() + index + 1, _body.end());
    if (index + 1 == _body.size()) {
        _body.erase(_body.begin() + index);
    } else {
        _body.erase(_body.begin() + index, _body.end());
    }
    return (arcade::CentipedeGame::CentipedeEntity(newBodyTail, _currentDirection));
}

arcade::Vector arcade::CentipedeGame::CentipedeEntity::getPosInFront() const
{
    if (_body.size() == 0)
        return (Vector(0, 0));

    Vector pos = _body.at(0);

    pos = addDirectionToVector(pos, _currentDirection);
    return (pos);
}

void arcade::CentipedeGame::CentipedeEntity::move(arcade::CentipedeGame::CellType cellInFront)
{
    if (_body.size() == 0)
        return;

    Vector headPos;
    if (cellInFront == OBSTACLE) {
        headPos = addDirectionToVector(_body.at(0), DOWN);
        _body.insert(_body.begin(), headPos);
        _body.pop_back();
        switch (_currentDirection) {
            case LEFT:
                _currentDirection = RIGHT;
                break;
            case RIGHT:
                _currentDirection = LEFT;
                break;
            default:
                break;
        }
    } else {
        headPos = addDirectionToVector(_body.at(0), _currentDirection);
        _body.insert(_body.begin(), headPos);
        _body.pop_back();
    }
}

arcade::Vector arcade::CentipedeGame::CentipedeEntity::addDirectionToVector(const arcade::Vector &pos,
                                                                        arcade::CentipedeGame::Direction direction)
{
    Vector newPos = pos;

    switch (direction) {
        case UP:
            newPos.y -= 1;
            break;
        case DOWN:
            newPos.y += 1;
            break;
        case LEFT:
            newPos.x -= 1;
            break;
        case RIGHT:
            newPos.x += 1;
            break;
        default:
            newPos.x = 0;
            newPos.y = 0;
            break;
    }
    return (newPos);
}

