/*
** EPITECH PROJECT, 2019
** Box.hpp
** File description:
** Box.hpp
*/

#ifndef OOP_ARCADE_2018_BOX_HPP
#define OOP_ARCADE_2018_BOX_HPP

#include "../../commons/include/IGame.hpp"

namespace arcade
{
    namespace CentipedeGame
    {
        class Box
        {
        public:
            Box(Vector const &pos);

            int getHealth() const { return (_health); }
            void decreaseHealth(int damage) { _health -= damage; }

            Vector const &getPos() const { return (_pos); }
            void setPos(Vector const &pos) { _pos = pos; }

        private:
            int _health;
            Vector _pos;
        };
    }
}

#endif //OOP_ARCADE_2018_BOX_HPP
