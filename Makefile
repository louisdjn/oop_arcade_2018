##
## EPITECH PROJECT, 2019
## arcade
## File description:
## Makefile
##

all:
		@make all --no-print-directory -C lib
		@make all --no-print-directory -C games
		@make all --no-print-directory -C core

clean:
		@make clean --no-print-directory -C lib
		@make clean --no-print-directory -C games
		@make clean --no-print-directory -C core

fclean:
		@make fclean --no-print-directory -C lib
		@make fclean --no-print-directory -C games
		@make fclean --no-print-directory -C core

re:
		@make re --no-print-directory -C lib
		@make re --no-print-directory -C games
		@make re --no-print-directory -C core

games:
		@make all --no-print-directory -C games

core:
		@make all --no-print-directory -C core

graphicals:
		@make all --no-print-directory -C lib

.PHONY: all clean fclean re games core graphicals